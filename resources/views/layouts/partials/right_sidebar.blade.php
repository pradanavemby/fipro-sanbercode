<div class="col-md-4  mt-5 pt-4 pr-0">

  <div class="card rounded" style="background: #c77ff2;  position: -webkit-sticky; position: sticky; top: 60px;">
    <div class="card-header">
      <h3 class="card-title">Trends for you</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <ul class="products-list product-list-in-card pr-2">

        <li class="item pl-4 py-1" style="background: #c77ff2;">
          <a href="#" class="product-title text-dark"># Hashtag</a>
          <p>45k Tweets</p>
        </li>
        <li class="item pl-4 py-1" style="background: #c77ff2;">
          <a href="#" class="product-title text-dark"># Hashtag</a>
          <p>45k Tweets</p>
        </li>
        <li class="item pl-4 py-1" style="background: #c77ff2;">
          <a href="#" class="product-title text-dark"># Hashtag</a>
          <p>45k Tweets</p>
        </li>

      </ul>
    </div>
    <!-- /.card-body -->
  </div>

  <div class="card" style="background: #c77ff2;  position: -webkit-sticky; position: sticky; top: 290px;">
    <div class="card-header">
      <h3 class="card-title">Who to follow</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <ul class="products-list product-list-in-card pl-3 pr-2">

        <li class="item" style="background: #c77ff2;">
          <div class="product-img">
            <img class="img-circle img-size-50" src="{{ asset('images/profile-image.jpg') }}" alt="User Image">  
          </div>
          <div class="product-info">
            <a href="#" class="product-title text-dark">name</a>
            <span><a class="btn btn-light btn-sm float-right mt-2 mr-3 py-2" href="#" style="border-radius:50px">Follow</a></span>
            <p>@username</p>     
          </div>
        </li>
        <li class="item" style="background: #c77ff2;">
          <div class="product-img">
            <img class="img-circle img-size-50" src="{{ asset('images/profile-image.jpg') }}" alt="User Image">  
          </div>
          <div class="product-info">
            <a href="#" class="product-title text-dark">name</a>
            <span><a class="btn btn-light btn-sm float-right mt-2 mr-3 py-2" href="#" style="border-radius:50px">Follow</a></span>
            <p>@username</p>     
          </div>
        </li>
        <li class="item" style="background: #c77ff2;">
          <div class="product-img">
            <img class="img-circle img-size-50" src="{{ asset('images/profile-image.jpg') }}" alt="User Image">  
          </div>
          <div class="product-info">
            <a href="#" class="product-title text-dark">name</a>
            <span><a class="btn btn-light btn-sm float-right mt-2 mr-3 py-2" href="#" style="border-radius:50px">Follow</a></span>
            <p>@username</p>     
          </div>
        </li>

      </ul>         
    </div>
  </div>

  <div class="description mb-3" style="position: -webkit-sticky; position: sticky; top: 565px;">
    <p class="text-dark">Copyright &copy; 2020 Twitter All rights reserved.</p>
  </div>
</div>